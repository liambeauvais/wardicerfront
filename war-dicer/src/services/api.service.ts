import {Observable, from, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators'
import {environment} from "app/environment";
import {data} from "autoprefixer";
import axios from "axios";

export class ApiService {
  private _apiUrl: string = environment.dev? environment.devApiUrl : environment.prodApiUrl;

  get(path: string): Observable<any> {
    return from(
      axios.get(`${this._apiUrl}/api/profiles/`)
    ).pipe(
      map(
        response => {
          return response.data
        }
      ),
      catchError(
        (error) => {
          console.error("Erreur sur get", error)
          return throwError(error)
        }
      )
    )
  }

  postAlgorithm(data: any) {
    return axios.post(`${this._apiUrl}/api/profile/get_statistics/`, data)
  }
}
