import MainLayout from "layouts/MainLayout.vue";
import ErrorNotFound from "pages/ErrorNotFound.vue";

const routes = [
  {
    path: '/',
    component: ()=>import('layouts/MainLayout.vue'),
    children: [
      { path: 'simulator', component: () => import('pages/Simulator.vue') },
      { path: '', redirect: 'simulator' },
      { path: '/:pathMatch(.*)*', redirect: 'simulator' },

    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:pathMatch(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
